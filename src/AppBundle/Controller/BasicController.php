<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BasicController
 *
 * @Route("/page")
 */
class BasicController extends Controller
{
    /**
     * @Route("/my_profile", name="myProfile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myPageAction()
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $images = $currentUser->getImages();

        $form = $this->createForm(PostType::class, new Image());
        return $this->render('@App/Basic/myPage.html.twig', [
            'form' => $form->createView(),
            'images' => $images,
            'user' => $currentUser
        ]);
    }

    /**
     * @Route("/like/{id}", name="addLike", requirements={"id": "\d+"})
     * @param int $id
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doLikeAction(int $id, EntityManagerInterface $em)
    {
        $currentUser = $this->getUser();
        $repository = $this->getDoctrine()->getRepository(Image::class);
        $image = $repository->find($id);
        if ($image->hasLiker($currentUser)){
            $image->removeLiker($currentUser);
        }else{
            $image->addLiker($currentUser);
        }
        $em->persist($image);
        $em->flush();
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/follow/{id}", name="follow", requirements={"id": "\d+"})
     * @param int $id
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addFollowAction(int $id, EntityManagerInterface $em)
    {
        $currentUser = $this->getUser();
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->find($id);

        if ($user->hasFollow($currentUser)){
            $user->removeFollow($currentUser);
        }else{
            $user->addFollow($currentUser);
        }
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/followers", name="followers")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function followsAction()
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        /** @var ArrayCollection $followers */
        $followers = $currentUser->getFollowers();

        return $this->render('@App/Basic/my_follows.html.twig', [
            'images' => $followers
        ]);
    }

    /**
     * @Route("/create", name="create_post")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createNewPostAction(Request $request)
    {
        $post = new Image();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $post->setUser($this->getUser());
            $em->persist($post);
            $em->flush();
            return $this->redirectToRoute('myProfile');
        }
        return $this->render('@App/Basic/myPage.html.twig', array(
            'form' => $form->createView(),
        ));


    }
}
